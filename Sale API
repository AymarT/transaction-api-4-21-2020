---
title: "Sale API Call"
date: 2020-04-03T10:02:25-07:00
draft: true
---

<p>The Sale API is called once the transaction has been completed by the end consumer.
This call is used to identify the transaction as successful so that it can then be marked as complete. The Sale API is called using the mTxId and/or the sTxId (or both) to identify the transaction.
The Sale API call will return the sTxId in response to confirm the transaction has been updated.</p>
<p>Please note: The update of the mTxId is allowed in this API call.</P>

<br>
  
  
 <span style="color: #1B5FDD"/> <font size="+2">ENDPOINT</font></span>
     

       https://api-test.interpayments.com/v1/ch/sale

<br>
<p style + "font-Ariel:">For Testing purposes you may want to use our <span style= "color: #1B5FDD"/><a href="http://localhost:1313/basic-workflow/b/"><u>Test Cards</u></span></p></a>      
<br>
<br>
<p><span style="color: #1B5FDD"/> <font size="+2">HEADERS</font></span></p>

   
       Content-Type : application/json
    X-Requested-With : xhr
    Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NDA5MjcyNzgsImNsaWVudElkIjoiQ1JXIiwiaWQiOiJhMWQxODRiYy1lMzk5LTRiOTItYjFjNy0zY2RkMzM4YzIxMzEifQ.viBGNeKOG57nbZg1rCJbXf3H6QfM3TsEjx7QMkA3R90
        
<br>
<p><span style="color: #1B5FDD"/> <font size="+2">REQUEST BODY</font></span></p>

             {
        "sTxId": "5ml635gste9log31t5llq2akg",
        "authCode": "optional notes/order details"
        }
<br>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial;}
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}
.tab button:hover {
  background-color: #ddd;
}
.tab button.active {
  background-color: #ccc;
}
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>
</head>
<body>

<h2></h2>
<span style="color: #1B5FDD"/> <font size="+2">Click on the buttons inside the tabbed menu:</font></span>

<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'curl')">curl</button>
  <button class="tablinks" onclick="openCity(event, 'node')">node</button>
  <button class="tablinks" onclick="openCity(event, 'jQuery')">jQuery</button>
  <button class="tablinks" onclick="openCity(event,  'Ruby')">Ruby</button>
  <button class="tablinks" onclick="openCity(event,  'Python Request')">Python Request</button>
  <button class="tablinks" onclick="openCity(event,   'PHP')">PHP</button>
  <button class="tablinks" onclick="openCity(event,   'Go')">Go</button>
</div>

<div id="curl" class="tabcontent">
  <h3>curl</h3>
  <p>  --location --request POST "https://api-test.interpayments.com/v1/ch/sale" \
        --header "Content-Type: application/json" \
        --header "X-Requested-With: xhr" \
        --data "{
            \"sTxId\": \"Insert sTxId here\",
            \"authCode\": \"optional notes/order details\"
        }"</p>
</div>

<div id="node" class="tabcontent">
  <h3>node</h3>
  <p>var https = require('https');

var options = {
    "method": "POST",
    "hostname": "api-test.interpayments.com",
    "path": "/v1/ch/sale",
    "headers": {
        "Content-Type": "application/json",
        "X-Requested-With": "xhr"
    }
};

var req = https.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function (chunk) {
        var body = Buffer.concat(chunks);
        console.log(body.toString());
    });

    res.on("error", function (error) {
        console.error(error);
    });
});

var postData =  "{ \"sTxId\": \"Insert sTxId here\"\n, \"authCode\": \"optional notes/order details\"}";

req.write(postData);

req.end();
RESPONSE
{ 
"sTxId": "5ml635gste9log31t5llq2akg"
}

var req = https.request(options, function (res) {
    var chunks = [];

    res.on("data", function (chunk) {
        chunks.push(chunk);
    });

    res.on("end", function (chunk) {
        var body = Buffer.concat(chunks);
        console.log(body.toString());
    });

    res.on("error", function (error) {
        console.error(error);
    });
});

var postData =  "{ \"sTxId\": \"Insert sTxId here\"\n, \"mTxId\": \"a-merchant-unique-transaction-id\"}";

req.write(postData);

req.end();
</p> 
</div>

<div id="jQuery" class="tabcontent">
  <h3>jQuery</h3>
  <p>var settings = {
   "url": "https://api-test.interpayments.com/v1/ch/sale",
   "method": "POST",
   "timeout": 0,
   "headers": {
        "Content-Type": "application/json",
        "X-Requested-With": "xhr"
   },
   "data": { "sTxId": "Insert sTxId here", "authCode": "optional notes/order details"},
};

$.ajax(settings).done(function (response) {
    console.log(response);
});</p>
</div>
<div id="Ruby" class="tabcontent">
  <h3>Ruby</h3>
  <p>  require "uri"
require "net/http"

url = URI("https://api-test.interpayments.com/v1/ch/sale")

https = Net::HTTP.new(url.host, url.port)
https.use_ssl = true

request = Net::HTTP::Post.new(url)
request["Content-Type"] = "application/json"
request["X-Requested-With"] = "xhr"
request.body = "{ "sTxId": "Insert sTxId here", "authCode": "optional notes/order details"}"

response = https.request(request)
puts response.read_body</p>
</div>
<div id="Python Request" class="tabcontent">
  <h3>Python Request</h3>
  <p>import requests
url = 'https://api-test.interpayments.com/v1/ch/sale'
payload = "{ \n\"sTxId\": \"Insert sTxId here\"\n, \"authCode\": \"optional notes/order details\"}"
headers = {
    'Content-Type': 'application/json',
    'X-Requested-With': 'xhr'
}
response = requests.request('POST', url, headers = headers, data = payload, allow_redirects=False)
print(response.text)</p>
</div>
<div id="PHP" class="tabcontent">
  <h3>PHP</h3>
  <p>       <?php
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api-test.interpayments.com/v1/ch/sale",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => false,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS =>"{ "sTxId": "Insert sTxId here", "authCode": "optional notes/order details"}",
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "X-Requested-With: xhr"
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }
        ?>
</p>
</div>
<div id="Go" class="tabcontent">
  <h3>Go</h3>
  <p>  package main

import (
    "fmt"
    "strings"
    "os"
    "path/filepath"
    "net/http"
    "io/ioutil"
)

func main() {

url := https://api-test.interpayments.com/v1/ch/sale
method := "POST"

payload := strings.NewReader("{
    "sTxId": "5ml635gste9log31t5llq2akg",
    "authCode": "optional notes/order details"
}")

client := &http.Client {
    CheckRedirect: func(req *http.Request, via []*http.Request) error {
        return http.ErrUseLastResponse
    },
}
req, err := http.NewRequest(method, url, payload)

if err != nil {
    fmt.Println(err)
}
req.Header.Add("Content-Type", "application/json")
req.Header.Add("X-Requested-With", "xhr")

res, err := client.Do(req)
defer res.Body.Close()
body, err := ioutil.ReadAll(res.Body)

fmt.Println(string(body))
}</p>
</div>



<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
   
</body>
</html> 
<br>
<p><span style="color: #1B5FDD"/> <font size="+2">Response</font></span><p>               
    
    { 
        "sTxId": "5ml635gste9log31t5llq2akg"
    }                     
<br>
<table>
<tr>
   <h2><span style="color: #1B5FDD"/> <font size="+2">Nomenclature</font></span></center>  </h2>
  <tr>
    <th><u>Name</u></th>
    <th><u>Information</u></th>
    <th><u>Mandatory</u></th>
  </tr>
  <tr>
  <td>mTxId</td>
  <td>	A Merchant orderId or unique identifier in the Merchant e-commerce scope referencing the Order or Shopping Cart. This field is optional.</td>
  <td>Yes, if sTxId is not passed
</td>
  </tr>
  <tr>
  <td>sTxId</td>
  <td>A unique identifier in the InterPayments system which refers to the persistence state of the transaction Fee calculation.</td>
  <td>Yes, if mTxId is not passed
</td>
  </tr>
  <td>authCode</td>
  <td>	An opaque data field that can be used to store an authorization code returned from the Merchant’s credit card processing process; or any other data representing the Merchant's transaction</td>
  </tr>

   
  </table>
<br>
<br>
<br>
<br>
<br>